const path = require('path');
const { chewDir } = require('chew-dir');
const prompts = require('prompts');
const { Questions } = require('./questions');
const { removeExamples, removeExampleComments } = require('./examples');
const { mapFile, pipe, slugify, underscoresToDotFile } = require('./utils');

module.exports.initApp = async function initApp({ target }) {
  console.log('A few questions for your package.json');
  const {
    moveOn,
    keepExplanations,
    keepExamples = keepExplanations,
    ...responses
  } = await prompts(Questions({ target }));

  if (!moveOn) { return; }
  await generateFiles({ target, keepExplanations, keepExamples, ...responses });
  console.log("Ready");
  console.log('Run "npm install", then "npm start" to start your app');
  console.log('See "npm run" for further options');
};

function generateFiles({ target, keepChangelog, keepReadme, ...responses }) {
  return chewDir({
    target,
    source: path.resolve(__dirname, '../template'),
    deep: true,
    exclude: [
      optionally(!keepChangelog, 'CHANGELOG.md'),
      optionally(!keepReadme, 'README.md'),
    ].filter(Boolean),
    process: processWith(responses),
  });
}

function processWith({ keepExplanations, keepExamples, ...responses }) {
  const mapContent = pipe(...[
    optionally(!keepExamples, removeExamples),
    optionally(!keepExplanations, removeLineComments),
    removeExampleComments,
  ].filter(Boolean));

  const outputCopy = outputWith({ mapBase: underscoresToDotFile });
  const outputSource = outputWith({ mapBase: underscoresToDotFile, mapContent });

  return async (file, api) => {
    if (file.relativePath === 'package.json') {
      return outputPackageJson(responses)(file, api);
    }
    if (['.js', '.ts', '.scss', '.vue', '.json'].includes(file.ext)) {
      return outputSource(file, api);
    }
    return outputCopy(file, api);
  };
}

function outputPackageJson({
  appName = 'scaffolding',
  appDescription = '',
  appVersion = '0.0.1',
  authorName = '',
  apiPath = '/',
  publicPath = '/',
}) {
  return async ({ read }, { outputFile }) => {
    const packageContent = await read();
    return outputFile('package.json', packageContent
        .replace('vue_app_scaffolding_name', slugify(appName))
        .replace('vue_app_scaffolding_description', appDescription)
        .replace('vue_app_scaffolding_author', authorName)
        .replace('/vue_app_scaffolding_api_path/', withTrailingSlash(apiPath))
        .replace('/vue_app_scaffolding_public_path/', withTrailingSlash(publicPath))
        .replace(/"version": ".*"/, `"version": "${appVersion}"`));
  };
}

function withTrailingSlash(path = '') {
  return path.endsWith('/') ? path : `${path}/`;
}

function outputWith({ mapBase, mapContent }) {
  return async (file, { outputFile }) => {
    const { path, content } = await mapFile({ base: mapBase, content: mapContent })(file);
    if (typeof content !== 'string') { return; }
    return outputFile(path, content);
  };
}

function optionally(condition, value) {
  return condition ? value : null;
}

function removeLineComments(content) {
  if (typeof content !== 'string') { return content; }
  return content.replace(/^[\t ]*\/\/.*$\r?\n/gm, '');
}
