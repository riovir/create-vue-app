const { pipe, removeMatch } = require('../utils');
const {
  BLOCK_DISABLE, BLOCK_ENABLE, FILE_DELETE, LINE_DISABLE,
  toLineHavingBlockRegExp, toJsonHavingBlockRegExp, toLineCommentRegex
} = require('./comment-types');

module.exports.removeExampleComments = pipe(
  removeMatch(toLineHavingBlockRegExp(FILE_DELETE)),
  removeMatch(toJsonHavingBlockRegExp(FILE_DELETE, '__comment__')),
  removeMatch(toLineHavingBlockRegExp(BLOCK_DISABLE)),
  removeMatch(toLineHavingBlockRegExp(BLOCK_ENABLE)),
  removeMatch(toLineCommentRegex(LINE_DISABLE)),
);
