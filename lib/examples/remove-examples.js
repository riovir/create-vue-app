const { pipe, removeMatch } = require('../utils');
const {
  BLOCK_DISABLE, BLOCK_ENABLE, FILE_DELETE, LINE_DISABLE,
  toLineHavingInlineRegExp, toIncludeBlockRegex, toJsonHavingBlockRegExp
} = require('./comment-types');
const { applyCustomFixes } = require('./apply-custom-fixes');


module.exports.removeExamples = pipe(
  applyCustomFixes,
  nullifyMatch(toJsonHavingBlockRegExp(FILE_DELETE)),
  nullifyMatch(toIncludeBlockRegex(FILE_DELETE)),
  removeMatch(toLineHavingInlineRegExp(LINE_DISABLE)),
  removeBlocks({
    blockStart: toIncludeBlockRegex(BLOCK_DISABLE),
    blockEnd: toIncludeBlockRegex(BLOCK_ENABLE),
  }),
);

function nullifyMatch(regExp) {
  return value => regExp.test(value) ? null : value;
}

function removeBlocks({ blockStart, blockEnd }) {
  return content => {
    if (typeof content !== 'string') { return content; }
    const newLine = newLineOf(content);
    const { result } = content.split(newLine).reduce(
        withoutBlocks({ newLine, blockStart, blockEnd }),
        { result: '', blockDepth: 0 });
    return result;
  };
}

function withoutBlocks({ newLine, blockStart, blockEnd }) {
  return ({ result, blockDepth }, line) => {
    if (blockStart.test(line)) {
      return { result, blockDepth: blockDepth + 1 };
    }
    if (blockEnd.test(line)) {
      return { result, blockDepth: blockDepth - 1 };
    }
    if (0 < blockDepth) {
      return { result, blockDepth };
    }
    const newResult = result ? `${result}${newLine}${line}` : line;
    return { result: newResult, blockDepth };
  };
}

function newLineOf(string) {
  if (string.includes('\r\n')) { return '\r\n'; }
  return string.includes('\r') ? '\r' : '\n';
}
