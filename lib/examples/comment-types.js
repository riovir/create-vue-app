module.exports.BLOCK_DISABLE = 'scaffolding-disable unless keepExamples';
module.exports.BLOCK_ENABLE = 'scaffolding-enable';
module.exports.FILE_DELETE = 'scaffolding-delete-file unless keepExamples';
module.exports.LINE_DISABLE = 'scaffolding-disable-line unless keepExamples';

const GAP = '[\t ]*';
const LINE_BREAK = '$\\r?\\n';
const LINE_COMMENT = `${GAP}(\\/\\/)${GAP}`;
const BLOCK_COMMENT_START = `${GAP}(<!--|\\/\\*)${GAP}`;
const BLOCK_COMMENT_END = `${GAP}(-->|\\*\\/)${GAP}`;
const asBlock = name => `${BLOCK_COMMENT_START}${name}${BLOCK_COMMENT_END}`;

module.exports.toLineHavingBlockRegExp = function toLineHavingBlockRegExp(name) {
  return new RegExp(`^.*${asBlock(name)}.*${LINE_BREAK}`, 'gm');
};

module.exports.toLineHavingInlineRegExp = function toLineHavingInlineRegExp(name) {
  return new RegExp(`^.*${LINE_COMMENT}${name}.*${LINE_BREAK}`, 'gm');
};

module.exports.toJsonHavingBlockRegExp = function toJsonHavingBlockRegExp(name, prop) {
  return new RegExp(`^${GAP}"${prop}"${GAP}:${GAP}"${asBlock(name)}",?${LINE_BREAK}`, 'gm');
};

module.exports.toLineCommentRegex = function toLineCommentRegex(name) {
  return new RegExp(`${LINE_COMMENT}${name}.*`, 'g');
};

module.exports.toIncludeBlockRegex = function toIncludeRegex(name) {
  return new RegExp(`${asBlock(name)}`);
};
