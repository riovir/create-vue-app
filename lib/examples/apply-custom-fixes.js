/* eslint-disable max-len */

/** XXX: Should find a more sustainable way to ensure
 * no linter issues when the examples are stripped off */
module.exports.applyCustomFixes = function applyCustomFixes(content, { base }) {
  if (base === 'the-app.vue') {
    return content.replace(
      '<div class="page container">',
      '<div class="page container">\n\t\tWelcome' );
  }
  if (base === 'the-app.spec.ts') {
    return content.replace(
      `import { byTestId, setupOptions, setupStore } from 'test/utils';`,
      `import { setupOptions, setupStore } from 'test/utils';`);
  }
  if (base === 'api.ts') {
    return content.replace(
      'export function Api({ get }: AxiosPartial = Axios()) {',
      'export function Api({ get }: AxiosPartial = Axios()) { // eslint-disable-line @typescript-eslint/no-unused-vars' );
  }
  if (base === 'store.ts') {
    return content
      .replace(
        'type State = typeof state;',
        'type State = typeof state; // eslint-disable-line @typescript-eslint/no-unused-vars')
      .replace(
        'type Getters = { [key in keyof typeof getters]: ReturnType<typeof getters[key]> };',
        'type Getters = { [key in keyof typeof getters]: ReturnType<typeof getters[key]> }; // eslint-disable-line @typescript-eslint/no-unused-vars')
      .replace(
        'export function Store({ api }: { api: Api }) {',
        'export function Store({ api }: { api: Api }) { // eslint-disable-line @typescript-eslint/no-unused-vars' );
  }
  if (base === 'dev-server.config.js') {
    return content.replace(
      `module.exports = function DevServerConfig({ publicPath = '/', apiPath = '/' } = {}) {`,
      `module.exports = function DevServerConfig({ publicPath = '/', apiPath = '/' } = {}) { // eslint-disable-line @typescript-eslint/no-unused-vars`,
    );
  }
  return content;
};
