module.exports.copyWith = function copyWith(mapName) {
  return ({ relativePath, relativeParent, base }, { copy }) => {
    const parent = relativeParent ? `${relativeParent}/` : '';
    return copy(relativePath, `${parent}${mapName(base)}`);
  };
};
