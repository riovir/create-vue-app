module.exports.mapFile = function mapFile({ base, content = same => same }) {
  return async (file) => {
    const { base: fileBase, read, relativeParent } = file;
    const parent = relativeParent ? `${relativeParent}/` : '';
    return {
      path: `${parent}${base(fileBase)}`,
      content: content(await read(), file)
    };
  };
};
