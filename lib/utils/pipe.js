module.exports.pipe = function pipe(fn, ...fns) {
  const applyOn = (result, fn) => fn(result);
  return (...args) => {
    return fns.reduce(applyOn, fn(...args));
  };
};
