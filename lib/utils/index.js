module.exports = {
  ...require('./copy-with'),
  ...require('./dotfiles'),
  ...require('./map-file'),
  ...require('./pipe'),
  ...require('./remove-match'),
  ...require('./slugify'),
};
