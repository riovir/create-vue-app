module.exports.dotFileToUnderscores = function dotFileToUnderscores(fileName) {
  return fileName.replace(/^\./, '__');
};

module.exports.underscoresToDotFile = function underscoresToDotFile(fileName) {
  return fileName.replace(/^__/, '.');
};
