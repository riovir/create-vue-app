module.exports.slugify = function slugify(string) {
  if (!string) { return string; }
  return string.trim().replace(/[\s()[\]'"]/g, '-');
};
