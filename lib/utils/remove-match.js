module.exports.removeMatch = function removeMatch(regexp) {
  return value => typeof value === 'string' ? value.replace(regexp, '') : value;
};
