#!/usr/bin/env node
const { initApp } = require('./init-app');

initApp({ target: process.cwd() });
