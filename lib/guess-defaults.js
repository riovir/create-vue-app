const path = require('path');
const fs = require('fs');
const iniparser = require('iniparser');
const { slugify } = require('./utils');

module.exports.guessDefaults = function guessDefaults({ target }) {
  const workingDirName = path.basename(target);
  const homeDir = process.platform === 'win32' ?
    process.env.USERPROFILE :
    process.env.HOME || process.env.HOMEPATH;
  const configFile = path.join(homeDir, '.gitconfig');
  const config = fs.existsSync(configFile) ? iniparser.parseSync(configFile) : {};
  const user = config.user ? config.user : {};

  return {
    appName: slugify(workingDirName),
    authorName: user.name || '',
    authorEmail: user.email || ''
  };
};
