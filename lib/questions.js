const fs = require('fs');
const { guessDefaults } = require('./guess-defaults');

module.exports.Questions = function Questions({ target }) {
  const dirEmpty = fs.readdirSync(target).length === 0;
  const { appName, authorName } = guessDefaults({ target });
  return [
    {
      type: 'text',
      name: 'appName',
      message: 'What is the name of your project?',
      initial: appName
    },
    {
      type: 'text',
      name: 'appDescription',
      message: 'What is the description?'
    },
    {
      type: 'text',
      name: 'appVersion',
      message: 'What is the version of your project?',
      initial: '0.0.1'
    },
    {
      type: 'text',
      name: 'authorName',
      message: 'What is the author name?',
      initial: authorName
    },
    {
      type: 'text',
      name: 'publicPath',
      message:
        'What is the public path of this project? Typically required when multiple apps are served by a single server. ' +
        `Eg: /${appName}/`,
      initial: '/'
    },
    {
      type: 'text',
      name: 'apiPath',
      message:
        'What is the API path of this project? The part of the URL that all API requests begin with, ending with a /. ' +
        `Eg: /${appName}/api/`,
      initial: '/'
    },
    {
      type: 'confirm',
      name: 'keepChangelog',
      message: 'Do you want to keep the original CHANGELOG.md of the scaffolding?',
      initial: false
    },
    {
      type: 'confirm',
      name: 'keepReadme',
      message: 'Do you want to keep the original README.md of the scaffolding?',
      initial: false
    },
    {
      type: 'select',
      name: 'keepExplanations',
      message: 'What about sample code and explanation comments?',
      choices: [
        { title: 'Select...', disabled: true },
        { title: 'Keep them', description: 'recommended for learning', value: true },
        { title: 'Remove them', description: 'if you are already familiar with them', value: false },
      ],
    },
    {
      type: 'confirm',
      name: 'moveOn',
      message: dirEmpty ? 'Continue?' : 'WARNING: the current directory is not empty. Are you sure you want to copy files here?',
      initial: dirEmpty
    }
  ];
};
