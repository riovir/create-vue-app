const { existsSync, mkdirSync } = require('fs');
const { resolve } = require('path');
const { initApp } = require('../lib/init-app');

const target = resolve(__dirname, 'target');

if (!existsSync(target)){
    mkdirSync(target);
}
initApp({ target });
