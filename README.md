# create-vue-app

> **[NO LONGER MAINTAINED]** Used to be an initializer for an opinionated Vue.js app scaffolding. [Read more about it here.](https://gitlab.com/riovir/vue-app-scaffolding/-/blob/ab9d56d7c0f720604a81e3cdac2f38ae522bda72/src/the-app.vue#L4-15)


## Scaffolding source and docs

The actual scaffolding used to be maintained right here: https://gitlab.com/riovir/vue-app-scaffolding
