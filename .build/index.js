const path = require('path');
const { echo, exec, which } = require('shelljs');
const { chewDir } = require('chew-dir');
const { copyWith, dotFileToUnderscores } = require('../lib/utils');

update();

async function update() {
  const { location } = await downloadRepo({
    target: '../temp',
    provider: 'gitlab',
    url: 'gitlab.com:riovir/vue-app-scaffolding#master',
    httpsUrl: 'https://gitlab.com/riovir/vue-app-scaffolding.git'
  });

  return chewDir({
    source: location,
    target: path.resolve(location, '../template'),
    deep: true,
    exclude: [
      'package-lock.json',
      '.git/'
    ],
    process: callBoth(
      copyWith(dotFileToUnderscores),
      overrideOwnChangelog
    )
  });
}

async function downloadRepo({ target, httpsUrl }) {
  if (!which('git')) {
    echo('Sorry, this script requires git');
    exit(1);
  }
  const targetPath = path.resolve(__dirname, target);
  if (exec(`git clone ${httpsUrl} ${targetPath}`).code !== 0) {
    echo('Error: Git clone failed');
    exit(1);
  }
  return { location: targetPath };
}

async function overrideOwnChangelog({ relativePath, read }, { outputFile }) {
  if (relativePath.toLowerCase() !== 'changelog.md') { return; }
  return outputFile('../CHANGELOG.md', await read());
}

function callBoth(fnOne, fnTwo) {
  return (...args) => Promise.all([
    fnOne(...args),
    fnTwo(...args)
  ]);
}
